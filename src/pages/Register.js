/*
	import:
		react, useState, useEffect from react

		Form, Container, Button from react-bootstrap
*/
// Base Imports
import	React, { useState, useEffect, useContext } from 'react';
import	{ Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../userContext.js';

// Bootstrap
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'; //npm install sweetalert2 to install sweetalert2

export default function Register(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [age, setAge] = useState();
	const [gender, setGender] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);	

/*
	TWO_WAY BINDING

		To be able to capture/save the input value from the input elements, we can bind the value of the element with the states. We, as devs, cannot type into the the inputs anymore because there is now value that is bound to it. We will add an onChange event to be able to update the state that is bound to the input

		Two-way binding is done so that we can assure that we can save the input into our states as the users type into the element. This is so that we don't have to save it before submitting.

		"e.target.value"
		e - the event to which the element will listen
		target - the element where the event will happen
		value - the value that the user has entered in that element

*/



// Function to simulate user registration
function registerUser(e) {
	// Prevents page redirection via form submission
	e.preventDefault();
	fetch('http://localhost:4000/api/users/checkEmail', {
	    method: "POST",
	    headers: {
	        'Content-Type': 'application/json'
	    },
	    body: JSON.stringify({
	        email: email
	    })
	})
	.then(res => res.json())
	.then(data => {

	    console.log(data);

	    if(data === true){

	    	Swal.fire({
	    		title: 'Duplicate email found',
	    		icon: 'error',
	    		text: 'Kindly provide another email to complete the registration.'	
	    	});

	    }
	    else{
	    	fetch('http://localhost:4000/api/users/register', {
	    		method: "POST",
	    		headers: {
	    		    'Content-Type': 'application/json'
	    		},
	    		body: JSON.stringify({
	    		    firstName: firstName,
	    		    lastName: lastName,
	    		    age: age,
	    		    gender: gender,
	    		    email: email,
	    		    mobileNo: mobileNo,
	    		    password: password1
	    		})

	    	})
	    	.then(res => res.json())
	    	.then(data => {
	    		console.log(data);

	    		if (data === true) {

	    		    // Clear input fields
	    		    setFirstName('');
	    		    setLastName('');
	    		    setAge('')
	    		    setGender('');
	    		    setEmail('');
	    		    setMobileNo('');
	    		    setPassword1('');
	    		    setPassword2('');

	    		    Swal.fire({
	    		        title: 'Registration successful',
	    		        icon: 'success',
	    		        text: 'Welcome to Zuitt!'
	    		    });

	    		    navigate("/login");

	    		} else {

	    		    Swal.fire({
	    		        title: 'Something wrong',
	    		        icon: 'error',
	    		        text: 'Please try again.'   
	    		    });

	    		}
	    	})
	    }


	})

}

	useEffect(() => {
		// Validation to enable the submit buttion when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && age !== '' && gender !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, age, gender, email, mobileNo, password1, password2])

	return (
		(user.access !== null) ?
		    <Navigate to="/courses" />
		:
		<Container>
			<h1>Register</h1>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="firstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter first name"
				        value={firstName} 
				        onChange={e => setFirstName(e.target.value)}
				        required
				    />
				</Form.Group>

				<Form.Group controlId="lastName">
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter last name"
				        value={lastName} 
				        onChange={e => setLastName(e.target.value)}
				        required
				    />
				</Form.Group>

				<Form.Group controlId="age">
				    <Form.Label>Age</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter your age"
				        value={age} 
				        onChange={e => setAge(e.target.value)}
				        required
				    />
				</Form.Group>

				<Form.Group controlId="gender">
				    <Form.Label>Gender</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter Gender"
				        value={gender} 
				        onChange={e => setGender(e.target.value)}
				        required
				    />
				</Form.Group>

			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group controlId="mobileNo">
			      <Form.Label>Mobile Number</Form.Label>
			      <Form.Control 
			          type="text" 
			          placeholder="Enter Mobile Number"
			          value={mobileNo} 
			          onChange={e => setMobileNo(e.target.value)}
			          required
			      />
			  </Form.Group>


			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange = { e => setPassword1(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  
			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange = { e => setPassword2(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			{/* Conditionally render submit button based on isActive state */}
			  {(isActive) ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
			  }
			  
			</Form>
		</Container>
	)
}

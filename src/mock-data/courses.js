export default [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut dui erat. Aenean a imperdiet mi. Curabitur eleifend ipsum sit amet fringilla hendrerit. Aliquam porttitor, magna quis feugiat convallis, mi nisl euismod nulla, id posuere ex mi nec felis.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut dui erat. Aenean a imperdiet mi. Curabitur eleifend ipsum sit amet fringilla hendrerit. Aliquam porttitor, magna quis feugiat convallis, mi nisl euismod nulla, id posuere ex mi nec felis.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut dui erat. Aenean a imperdiet mi. Curabitur eleifend ipsum sit amet fringilla hendrerit. Aliquam porttitor, magna quis feugiat convallis, mi nisl euismod nulla, id posuere ex mi nec felis.",
		price: 55000,
		onOffer: true
	}
]
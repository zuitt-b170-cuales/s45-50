 // dependencies
import React, { useState } from 'react';

 // bootstrap components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

/*
	Props - a short term for properties. Similar to the arguments/parameters found inside the functions. A way for the parent component to receive information.

		through the use of props, devs can use the same component and feed different information/data for rendering.
		.
		.
*/

export default function CourseCard(props) {
	let course = props.course;

	/*
		useState() used in React to allow components to create manage its own data and is meant to be used internally
			- accepts an argument that is meant to be the value of the first element in array

		in React.js, state values must not be changed directly. All changes to the state values must be through the setState function

		the enrollees will start at zero. the result of the useState() is an array of data that is then destructured into count and setCount

		setCount function is used to update the value of the count variable, depending on the times that the enroll function is triggered by the onClick command
	*/

	const [ count, setCount ] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll(){
		if(seats === 0){
			alert('No more seats.');
		}else{
			setCount (count + 1);
			setSeats(seats - 1);
		}
		
	}

	return(
		<Card>
			<Card.Body>
				<Card.Title>{course.name}</Card.Title>
				<h6>Description</h6>
				<p>{course.description}</p>
				<h6>Price</h6>
				<p>{course.price}</p>
				<h6>Enrollees</h6>
				<p>{count} Enrollees</p>
				<Button variant="primary" onClick={enroll}>Enroll</Button>
			</Card.Body>
		</Card>
		)
}

// ..
